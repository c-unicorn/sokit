/********************************************************************************
** Form generated from reading UI file 'transferform.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TRANSFERFORM_H
#define UI_TRANSFERFORM_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TransferForm
{
public:
    QVBoxLayout *lay1;
    QHBoxLayout *lay2;
    QGroupBox *box1;
    QVBoxLayout *lay5;
    QGridLayout *lay6;
    QLabel *lab2;
    QLabel *lab1;
    QComboBox *cmbSrcAddr;
    QComboBox *cmbSrcPort;
    QLabel *lab3;
    QComboBox *cmbDstAddr;
    QLabel *lab4;
    QComboBox *cmbDstPort;
    QToolButton *btnTrigger;
    QComboBox *cmbType;
    QGroupBox *box2;
    QHBoxLayout *lay8;
    QListWidget *lstConn;
    QVBoxLayout *lay9;
    QToolButton *btnConnAll;
    QToolButton *btnConnDel;
    QGridLayout *lay3;
    QLabel *lab5;
    QToolButton *btnSend1;
    QLabel *lab6;
    QLineEdit *edtBuf2;
    QToolButton *btnSend2;
    QLabel *lab7;
    QLineEdit *edtBuf3;
    QToolButton *btnSend3;
    QLineEdit *edtBuf1;
    QComboBox *cmbDir1;
    QComboBox *cmbDir2;
    QComboBox *cmbDir3;
    QHBoxLayout *lay4;
    QLabel *lab8;
    QLabel *lab9;
    QLabel *labRecv;
    QLabel *lab10;
    QLabel *labSend;
    QSpacerItem *spc;
    QCheckBox *chkLog;
    QToolButton *btnClear;
    QTreeWidget *treeOutput;
    QPlainTextEdit *txtOutput;

    void setupUi(QWidget *TransferForm)
    {
        if (TransferForm->objectName().isEmpty())
            TransferForm->setObjectName(QStringLiteral("TransferForm"));
        TransferForm->resize(680, 450);
        TransferForm->setMinimumSize(QSize(0, 0));
        TransferForm->setLocale(QLocale(QLocale::C, QLocale::AnyCountry));
        lay1 = new QVBoxLayout(TransferForm);
        lay1->setSpacing(5);
        lay1->setContentsMargins(5, 5, 5, 5);
        lay1->setObjectName(QStringLiteral("lay1"));
        lay2 = new QHBoxLayout();
        lay2->setObjectName(QStringLiteral("lay2"));
        box1 = new QGroupBox(TransferForm);
        box1->setObjectName(QStringLiteral("box1"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(box1->sizePolicy().hasHeightForWidth());
        box1->setSizePolicy(sizePolicy);
        box1->setMinimumSize(QSize(0, 93));
        box1->setMaximumSize(QSize(16777215, 93));
        lay5 = new QVBoxLayout(box1);
        lay5->setSpacing(5);
        lay5->setObjectName(QStringLiteral("lay5"));
        lay5->setContentsMargins(4, 0, 10, 5);
        lay6 = new QGridLayout();
        lay6->setObjectName(QStringLiteral("lay6"));
        lay6->setSizeConstraint(QLayout::SetFixedSize);
        lay6->setHorizontalSpacing(5);
        lay6->setVerticalSpacing(8);
        lab2 = new QLabel(box1);
        lab2->setObjectName(QStringLiteral("lab2"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(lab2->sizePolicy().hasHeightForWidth());
        lab2->setSizePolicy(sizePolicy1);
        lab2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        lay6->addWidget(lab2, 0, 2, 1, 1);

        lab1 = new QLabel(box1);
        lab1->setObjectName(QStringLiteral("lab1"));
        sizePolicy1.setHeightForWidth(lab1->sizePolicy().hasHeightForWidth());
        lab1->setSizePolicy(sizePolicy1);
        lab1->setMinimumSize(QSize(70, 0));
        lab1->setContextMenuPolicy(Qt::NoContextMenu);
        lab1->setTextFormat(Qt::PlainText);
        lab1->setScaledContents(false);
        lab1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        lay6->addWidget(lab1, 0, 0, 1, 1);

        cmbSrcAddr = new QComboBox(box1);
        cmbSrcAddr->setObjectName(QStringLiteral("cmbSrcAddr"));
        sizePolicy.setHeightForWidth(cmbSrcAddr->sizePolicy().hasHeightForWidth());
        cmbSrcAddr->setSizePolicy(sizePolicy);
        cmbSrcAddr->setMinimumSize(QSize(130, 24));
        cmbSrcAddr->setMaximumSize(QSize(130, 24));
        cmbSrcAddr->setEditable(true);
        cmbSrcAddr->setInsertPolicy(QComboBox::NoInsert);

        lay6->addWidget(cmbSrcAddr, 0, 1, 1, 1);

        cmbSrcPort = new QComboBox(box1);
        cmbSrcPort->setObjectName(QStringLiteral("cmbSrcPort"));
        sizePolicy.setHeightForWidth(cmbSrcPort->sizePolicy().hasHeightForWidth());
        cmbSrcPort->setSizePolicy(sizePolicy);
        cmbSrcPort->setMinimumSize(QSize(75, 24));
        cmbSrcPort->setMaximumSize(QSize(75, 24));
        cmbSrcPort->setEditable(true);
        cmbSrcPort->setInsertPolicy(QComboBox::InsertAtTop);

        lay6->addWidget(cmbSrcPort, 0, 3, 1, 1);

        lab3 = new QLabel(box1);
        lab3->setObjectName(QStringLiteral("lab3"));
        sizePolicy1.setHeightForWidth(lab3->sizePolicy().hasHeightForWidth());
        lab3->setSizePolicy(sizePolicy1);
        lab3->setMinimumSize(QSize(70, 0));
        lab3->setContextMenuPolicy(Qt::NoContextMenu);
        lab3->setTextFormat(Qt::PlainText);
        lab3->setScaledContents(false);
        lab3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        lay6->addWidget(lab3, 1, 0, 1, 1);

        cmbDstAddr = new QComboBox(box1);
        cmbDstAddr->setObjectName(QStringLiteral("cmbDstAddr"));
        sizePolicy.setHeightForWidth(cmbDstAddr->sizePolicy().hasHeightForWidth());
        cmbDstAddr->setSizePolicy(sizePolicy);
        cmbDstAddr->setMinimumSize(QSize(130, 24));
        cmbDstAddr->setMaximumSize(QSize(130, 24));
        cmbDstAddr->setEditable(true);
        cmbDstAddr->setInsertPolicy(QComboBox::NoInsert);

        lay6->addWidget(cmbDstAddr, 1, 1, 1, 1);

        lab4 = new QLabel(box1);
        lab4->setObjectName(QStringLiteral("lab4"));
        sizePolicy1.setHeightForWidth(lab4->sizePolicy().hasHeightForWidth());
        lab4->setSizePolicy(sizePolicy1);
        lab4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        lay6->addWidget(lab4, 1, 2, 1, 1);

        cmbDstPort = new QComboBox(box1);
        cmbDstPort->setObjectName(QStringLiteral("cmbDstPort"));
        sizePolicy.setHeightForWidth(cmbDstPort->sizePolicy().hasHeightForWidth());
        cmbDstPort->setSizePolicy(sizePolicy);
        cmbDstPort->setMinimumSize(QSize(75, 24));
        cmbDstPort->setMaximumSize(QSize(75, 24));
        cmbDstPort->setEditable(true);
        cmbDstPort->setInsertPolicy(QComboBox::InsertAtTop);

        lay6->addWidget(cmbDstPort, 1, 3, 1, 1);

        btnTrigger = new QToolButton(box1);
        btnTrigger->setObjectName(QStringLiteral("btnTrigger"));
        sizePolicy.setHeightForWidth(btnTrigger->sizePolicy().hasHeightForWidth());
        btnTrigger->setSizePolicy(sizePolicy);
        btnTrigger->setMinimumSize(QSize(90, 24));
        btnTrigger->setMaximumSize(QSize(90, 24));
        btnTrigger->setCheckable(true);

        lay6->addWidget(btnTrigger, 1, 4, 1, 1);

        cmbType = new QComboBox(box1);
        cmbType->insertItems(0, QStringList()
         << QStringLiteral("  -- TCP --")
         << QStringLiteral("  -- UDP --")
        );
        cmbType->setObjectName(QStringLiteral("cmbType"));
        sizePolicy.setHeightForWidth(cmbType->sizePolicy().hasHeightForWidth());
        cmbType->setSizePolicy(sizePolicy);
        cmbType->setMinimumSize(QSize(90, 24));
        cmbType->setMaximumSize(QSize(90, 24));

        lay6->addWidget(cmbType, 0, 4, 1, 1);


        lay5->addLayout(lay6);


        lay2->addWidget(box1);

        box2 = new QGroupBox(TransferForm);
        box2->setObjectName(QStringLiteral("box2"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(box2->sizePolicy().hasHeightForWidth());
        box2->setSizePolicy(sizePolicy2);
        box2->setMinimumSize(QSize(0, 93));
        box2->setMaximumSize(QSize(16777215, 93));
        lay8 = new QHBoxLayout(box2);
        lay8->setSpacing(5);
        lay8->setObjectName(QStringLiteral("lay8"));
        lay8->setContentsMargins(10, 5, 10, 10);
        lstConn = new QListWidget(box2);
        lstConn->setObjectName(QStringLiteral("lstConn"));
        sizePolicy2.setHeightForWidth(lstConn->sizePolicy().hasHeightForWidth());
        lstConn->setSizePolicy(sizePolicy2);
        lstConn->setMinimumSize(QSize(0, 58));
        lstConn->setMaximumSize(QSize(16777215, 58));
        lstConn->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        lstConn->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        lstConn->setEditTriggers(QAbstractItemView::NoEditTriggers);
        lstConn->setProperty("showDropIndicator", QVariant(false));
        lstConn->setSelectionMode(QAbstractItemView::MultiSelection);
        lstConn->setSelectionBehavior(QAbstractItemView::SelectRows);
        lstConn->setSelectionRectVisible(false);

        lay8->addWidget(lstConn);

        lay9 = new QVBoxLayout();
        lay9->setSpacing(10);
        lay9->setObjectName(QStringLiteral("lay9"));
        lay9->setContentsMargins(-1, 0, -1, -1);
        btnConnAll = new QToolButton(box2);
        btnConnAll->setObjectName(QStringLiteral("btnConnAll"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(btnConnAll->sizePolicy().hasHeightForWidth());
        btnConnAll->setSizePolicy(sizePolicy3);
        btnConnAll->setMinimumSize(QSize(0, 24));
        btnConnAll->setMaximumSize(QSize(16777215, 24));

        lay9->addWidget(btnConnAll);

        btnConnDel = new QToolButton(box2);
        btnConnDel->setObjectName(QStringLiteral("btnConnDel"));
        sizePolicy3.setHeightForWidth(btnConnDel->sizePolicy().hasHeightForWidth());
        btnConnDel->setSizePolicy(sizePolicy3);
        btnConnDel->setMinimumSize(QSize(0, 24));
        btnConnDel->setMaximumSize(QSize(16777215, 24));

        lay9->addWidget(btnConnDel);


        lay8->addLayout(lay9);


        lay2->addWidget(box2);


        lay1->addLayout(lay2);

        lay3 = new QGridLayout();
        lay3->setSpacing(5);
        lay3->setObjectName(QStringLiteral("lay3"));
        lay3->setContentsMargins(-1, 5, -1, -1);
        lab5 = new QLabel(TransferForm);
        lab5->setObjectName(QStringLiteral("lab5"));

        lay3->addWidget(lab5, 0, 0, 1, 1);

        btnSend1 = new QToolButton(TransferForm);
        btnSend1->setObjectName(QStringLiteral("btnSend1"));
        btnSend1->setMinimumSize(QSize(60, 24));
        btnSend1->setMaximumSize(QSize(60, 24));
        btnSend1->setCheckable(false);

        lay3->addWidget(btnSend1, 0, 3, 1, 1);

        lab6 = new QLabel(TransferForm);
        lab6->setObjectName(QStringLiteral("lab6"));

        lay3->addWidget(lab6, 1, 0, 1, 1);

        edtBuf2 = new QLineEdit(TransferForm);
        edtBuf2->setObjectName(QStringLiteral("edtBuf2"));
        edtBuf2->setMinimumSize(QSize(0, 24));
        edtBuf2->setMaximumSize(QSize(16777215, 24));

        lay3->addWidget(edtBuf2, 1, 1, 1, 1);

        btnSend2 = new QToolButton(TransferForm);
        btnSend2->setObjectName(QStringLiteral("btnSend2"));
        btnSend2->setMinimumSize(QSize(60, 24));
        btnSend2->setMaximumSize(QSize(60, 24));

        lay3->addWidget(btnSend2, 1, 3, 1, 1);

        lab7 = new QLabel(TransferForm);
        lab7->setObjectName(QStringLiteral("lab7"));

        lay3->addWidget(lab7, 2, 0, 1, 1);

        edtBuf3 = new QLineEdit(TransferForm);
        edtBuf3->setObjectName(QStringLiteral("edtBuf3"));
        edtBuf3->setMinimumSize(QSize(0, 24));
        edtBuf3->setMaximumSize(QSize(16777215, 24));

        lay3->addWidget(edtBuf3, 2, 1, 1, 1);

        btnSend3 = new QToolButton(TransferForm);
        btnSend3->setObjectName(QStringLiteral("btnSend3"));
        btnSend3->setMinimumSize(QSize(60, 24));
        btnSend3->setMaximumSize(QSize(60, 24));

        lay3->addWidget(btnSend3, 2, 3, 1, 1);

        edtBuf1 = new QLineEdit(TransferForm);
        edtBuf1->setObjectName(QStringLiteral("edtBuf1"));
        edtBuf1->setMinimumSize(QSize(0, 24));
        edtBuf1->setMaximumSize(QSize(16777215, 24));

        lay3->addWidget(edtBuf1, 0, 1, 1, 1);

        cmbDir1 = new QComboBox(TransferForm);
        cmbDir1->insertItems(0, QStringList()
         << QStringLiteral("S -> D")
         << QStringLiteral("D -> S")
        );
        cmbDir1->setObjectName(QStringLiteral("cmbDir1"));
        sizePolicy.setHeightForWidth(cmbDir1->sizePolicy().hasHeightForWidth());
        cmbDir1->setSizePolicy(sizePolicy);
        cmbDir1->setMinimumSize(QSize(0, 24));
        cmbDir1->setMaximumSize(QSize(16777215, 24));
        cmbDir1->setFrame(true);

        lay3->addWidget(cmbDir1, 0, 2, 1, 1);

        cmbDir2 = new QComboBox(TransferForm);
        cmbDir2->insertItems(0, QStringList()
         << QStringLiteral("S -> D")
         << QStringLiteral("D -> S")
        );
        cmbDir2->setObjectName(QStringLiteral("cmbDir2"));
        sizePolicy.setHeightForWidth(cmbDir2->sizePolicy().hasHeightForWidth());
        cmbDir2->setSizePolicy(sizePolicy);
        cmbDir2->setMinimumSize(QSize(0, 24));
        cmbDir2->setMaximumSize(QSize(16777215, 24));

        lay3->addWidget(cmbDir2, 1, 2, 1, 1);

        cmbDir3 = new QComboBox(TransferForm);
        cmbDir3->insertItems(0, QStringList()
         << QStringLiteral("S -> D")
         << QStringLiteral("D -> S")
        );
        cmbDir3->setObjectName(QStringLiteral("cmbDir3"));
        sizePolicy.setHeightForWidth(cmbDir3->sizePolicy().hasHeightForWidth());
        cmbDir3->setSizePolicy(sizePolicy);
        cmbDir3->setMinimumSize(QSize(0, 24));
        cmbDir3->setMaximumSize(QSize(16777215, 24));

        lay3->addWidget(cmbDir3, 2, 2, 1, 1);


        lay1->addLayout(lay3);

        lay4 = new QHBoxLayout();
        lay4->setSpacing(5);
        lay4->setObjectName(QStringLiteral("lay4"));
        lay4->setContentsMargins(0, 5, 0, 0);
        lab8 = new QLabel(TransferForm);
        lab8->setObjectName(QStringLiteral("lab8"));
        QSizePolicy sizePolicy4(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(lab8->sizePolicy().hasHeightForWidth());
        lab8->setSizePolicy(sizePolicy4);

        lay4->addWidget(lab8);

        lab9 = new QLabel(TransferForm);
        lab9->setObjectName(QStringLiteral("lab9"));

        lay4->addWidget(lab9);

        labRecv = new QLabel(TransferForm);
        labRecv->setObjectName(QStringLiteral("labRecv"));
        sizePolicy1.setHeightForWidth(labRecv->sizePolicy().hasHeightForWidth());
        labRecv->setSizePolicy(sizePolicy1);
#ifndef QT_NO_TOOLTIP
        labRecv->setToolTip(QStringLiteral(""));
#endif // QT_NO_TOOLTIP
        labRecv->setText(QStringLiteral("0"));

        lay4->addWidget(labRecv);

        lab10 = new QLabel(TransferForm);
        lab10->setObjectName(QStringLiteral("lab10"));

        lay4->addWidget(lab10);

        labSend = new QLabel(TransferForm);
        labSend->setObjectName(QStringLiteral("labSend"));
#ifndef QT_NO_TOOLTIP
        labSend->setToolTip(QStringLiteral(""));
#endif // QT_NO_TOOLTIP
        labSend->setText(QStringLiteral("0"));

        lay4->addWidget(labSend);

        spc = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        lay4->addItem(spc);

        chkLog = new QCheckBox(TransferForm);
        chkLog->setObjectName(QStringLiteral("chkLog"));

        lay4->addWidget(chkLog);

        btnClear = new QToolButton(TransferForm);
        btnClear->setObjectName(QStringLiteral("btnClear"));
        btnClear->setMinimumSize(QSize(60, 24));
        btnClear->setMaximumSize(QSize(60, 24));

        lay4->addWidget(btnClear);


        lay1->addLayout(lay4);

        treeOutput = new QTreeWidget(TransferForm);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setText(0, QStringLiteral("1"));
        treeOutput->setHeaderItem(__qtreewidgetitem);
        treeOutput->setObjectName(QStringLiteral("treeOutput"));
        QSizePolicy sizePolicy5(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(treeOutput->sizePolicy().hasHeightForWidth());
        treeOutput->setSizePolicy(sizePolicy5);
        treeOutput->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        treeOutput->setEditTriggers(QAbstractItemView::NoEditTriggers);
        treeOutput->setProperty("showDropIndicator", QVariant(false));
        treeOutput->setAlternatingRowColors(false);
        treeOutput->setSelectionMode(QAbstractItemView::SingleSelection);
        treeOutput->setSelectionBehavior(QAbstractItemView::SelectRows);
        treeOutput->setWordWrap(true);
        treeOutput->header()->setVisible(false);
        treeOutput->header()->setStretchLastSection(true);

        lay1->addWidget(treeOutput);

        txtOutput = new QPlainTextEdit(TransferForm);
        txtOutput->setObjectName(QStringLiteral("txtOutput"));
        sizePolicy2.setHeightForWidth(txtOutput->sizePolicy().hasHeightForWidth());
        txtOutput->setSizePolicy(sizePolicy2);
        txtOutput->setMinimumSize(QSize(0, 70));
        txtOutput->setMaximumSize(QSize(16777215, 70));
        txtOutput->setAcceptDrops(false);
        txtOutput->setFrameShape(QFrame::Box);
        txtOutput->setFrameShadow(QFrame::Plain);
        txtOutput->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        txtOutput->setTabChangesFocus(true);
        txtOutput->setUndoRedoEnabled(false);
        txtOutput->setReadOnly(true);
        txtOutput->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        lay1->addWidget(txtOutput);

        QWidget::setTabOrder(cmbSrcAddr, cmbSrcPort);
        QWidget::setTabOrder(cmbSrcPort, cmbDstAddr);
        QWidget::setTabOrder(cmbDstAddr, cmbDstPort);
        QWidget::setTabOrder(cmbDstPort, cmbType);
        QWidget::setTabOrder(cmbType, btnTrigger);
        QWidget::setTabOrder(btnTrigger, lstConn);
        QWidget::setTabOrder(lstConn, btnConnAll);
        QWidget::setTabOrder(btnConnAll, btnConnDel);
        QWidget::setTabOrder(btnConnDel, edtBuf1);
        QWidget::setTabOrder(edtBuf1, cmbDir1);
        QWidget::setTabOrder(cmbDir1, btnSend1);
        QWidget::setTabOrder(btnSend1, edtBuf2);
        QWidget::setTabOrder(edtBuf2, cmbDir2);
        QWidget::setTabOrder(cmbDir2, btnSend2);
        QWidget::setTabOrder(btnSend2, edtBuf3);
        QWidget::setTabOrder(edtBuf3, cmbDir3);
        QWidget::setTabOrder(cmbDir3, btnSend3);
        QWidget::setTabOrder(btnSend3, chkLog);
        QWidget::setTabOrder(chkLog, btnClear);
        QWidget::setTabOrder(btnClear, treeOutput);

        retranslateUi(TransferForm);

        QMetaObject::connectSlotsByName(TransferForm);
    } // setupUi

    void retranslateUi(QWidget *TransferForm)
    {
        TransferForm->setWindowTitle(QApplication::translate("TransferForm", "Transfer", Q_NULLPTR));
        box1->setTitle(QApplication::translate("TransferForm", "Network Setup", Q_NULLPTR));
        lab2->setText(QApplication::translate("TransferForm", "Port:", Q_NULLPTR));
        lab1->setText(QApplication::translate("TransferForm", "SRC Addr:", Q_NULLPTR));
        lab3->setText(QApplication::translate("TransferForm", "DST Addr:", Q_NULLPTR));
        lab4->setText(QApplication::translate("TransferForm", "Port:", Q_NULLPTR));
#ifndef QT_NO_STATUSTIP
        btnTrigger->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        btnTrigger->setText(QApplication::translate("TransferForm", "Start", Q_NULLPTR));
        box2->setTitle(QApplication::translate("TransferForm", "Connections", Q_NULLPTR));
        btnConnAll->setText(QApplication::translate("TransferForm", "All", Q_NULLPTR));
        btnConnDel->setText(QApplication::translate("TransferForm", "Disconn", Q_NULLPTR));
        lab5->setText(QApplication::translate("TransferForm", "Buf 1:", Q_NULLPTR));
        btnSend1->setText(QApplication::translate("TransferForm", "Send", Q_NULLPTR));
        lab6->setText(QApplication::translate("TransferForm", "Buf 2:", Q_NULLPTR));
        btnSend2->setText(QApplication::translate("TransferForm", "Send", Q_NULLPTR));
        lab7->setText(QApplication::translate("TransferForm", "Buf 3:", Q_NULLPTR));
        btnSend3->setText(QApplication::translate("TransferForm", "Send", Q_NULLPTR));
        lab8->setText(QApplication::translate("TransferForm", "Output:    ", Q_NULLPTR));
        lab9->setText(QApplication::translate("TransferForm", "Recv", Q_NULLPTR));
        lab10->setText(QApplication::translate("TransferForm", ", Send", Q_NULLPTR));
        chkLog->setText(QApplication::translate("TransferForm", "Write log", Q_NULLPTR));
        btnClear->setText(QApplication::translate("TransferForm", "Clear", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class TransferForm: public Ui_TransferForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TRANSFERFORM_H
