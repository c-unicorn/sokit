/********************************************************************************
** Form generated from reading UI file 'clientform.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLIENTFORM_H
#define UI_CLIENTFORM_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ClientForm
{
public:
    QVBoxLayout *vlayout1;
    QGroupBox *box1;
    QHBoxLayout *layout3;
    QLabel *lab1;
    QComboBox *cmbAddr;
    QLabel *lab2;
    QComboBox *cmbPort;
    QSpacerItem *spc1;
    QToolButton *btnTcp;
    QToolButton *btnUdp;
    QSpacerItem *spc2;
    QGridLayout *layout1;
    QLabel *lab3;
    QToolButton *btnSend0;
    QLabel *lab4;
    QToolButton *btnSend1;
    QLabel *lab5;
    QLineEdit *edtBuf2;
    QToolButton *btnSend2;
    QLabel *lab6;
    QLineEdit *edtBuf3;
    QToolButton *btnSend3;
    QLineEdit *edtBuf0;
    QLineEdit *edtBuf1;
    QHBoxLayout *layout2;
    QLabel *lab7;
    QLabel *lab8;
    QLabel *labRecv;
    QLabel *lab9;
    QLabel *labSend;
    QSpacerItem *spc3;
    QCheckBox *chkLog;
    QToolButton *btnClear;
    QTreeWidget *treeOutput;
    QPlainTextEdit *txtOutput;

    void setupUi(QWidget *ClientForm)
    {
        if (ClientForm->objectName().isEmpty())
            ClientForm->setObjectName(QStringLiteral("ClientForm"));
        ClientForm->resize(680, 450);
        ClientForm->setMinimumSize(QSize(0, 0));
        ClientForm->setLocale(QLocale(QLocale::C, QLocale::AnyCountry));
        vlayout1 = new QVBoxLayout(ClientForm);
        vlayout1->setSpacing(5);
        vlayout1->setContentsMargins(5, 5, 5, 5);
        vlayout1->setObjectName(QStringLiteral("vlayout1"));
        box1 = new QGroupBox(ClientForm);
        box1->setObjectName(QStringLiteral("box1"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(box1->sizePolicy().hasHeightForWidth());
        box1->setSizePolicy(sizePolicy);
        box1->setMinimumSize(QSize(0, 59));
        box1->setMaximumSize(QSize(16777215, 59));
        layout3 = new QHBoxLayout(box1);
        layout3->setSpacing(5);
        layout3->setObjectName(QStringLiteral("layout3"));
        layout3->setContentsMargins(5, 0, 5, 2);
        lab1 = new QLabel(box1);
        lab1->setObjectName(QStringLiteral("lab1"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(lab1->sizePolicy().hasHeightForWidth());
        lab1->setSizePolicy(sizePolicy1);
        lab1->setMinimumSize(QSize(0, 24));
        lab1->setMaximumSize(QSize(16777215, 24));
        lab1->setContextMenuPolicy(Qt::NoContextMenu);
        lab1->setTextFormat(Qt::PlainText);
        lab1->setScaledContents(false);

        layout3->addWidget(lab1);

        cmbAddr = new QComboBox(box1);
        cmbAddr->setObjectName(QStringLiteral("cmbAddr"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(cmbAddr->sizePolicy().hasHeightForWidth());
        cmbAddr->setSizePolicy(sizePolicy2);
        cmbAddr->setMinimumSize(QSize(130, 24));
        cmbAddr->setMaximumSize(QSize(130, 24));
        cmbAddr->setEditable(true);
        cmbAddr->setInsertPolicy(QComboBox::NoInsert);

        layout3->addWidget(cmbAddr);

        lab2 = new QLabel(box1);
        lab2->setObjectName(QStringLiteral("lab2"));
        sizePolicy1.setHeightForWidth(lab2->sizePolicy().hasHeightForWidth());
        lab2->setSizePolicy(sizePolicy1);
        lab2->setMinimumSize(QSize(0, 24));
        lab2->setMaximumSize(QSize(16777215, 24));

        layout3->addWidget(lab2);

        cmbPort = new QComboBox(box1);
        cmbPort->setObjectName(QStringLiteral("cmbPort"));
        sizePolicy2.setHeightForWidth(cmbPort->sizePolicy().hasHeightForWidth());
        cmbPort->setSizePolicy(sizePolicy2);
        cmbPort->setMinimumSize(QSize(75, 24));
        cmbPort->setMaximumSize(QSize(75, 24));
        cmbPort->setEditable(true);
        cmbPort->setInsertPolicy(QComboBox::InsertAtTop);

        layout3->addWidget(cmbPort);

        spc1 = new QSpacerItem(10, 0, QSizePolicy::Fixed, QSizePolicy::Minimum);

        layout3->addItem(spc1);

        btnTcp = new QToolButton(box1);
        btnTcp->setObjectName(QStringLiteral("btnTcp"));
        sizePolicy2.setHeightForWidth(btnTcp->sizePolicy().hasHeightForWidth());
        btnTcp->setSizePolicy(sizePolicy2);
        btnTcp->setMinimumSize(QSize(100, 24));
        btnTcp->setMaximumSize(QSize(100, 24));
        btnTcp->setCheckable(true);

        layout3->addWidget(btnTcp);

        btnUdp = new QToolButton(box1);
        btnUdp->setObjectName(QStringLiteral("btnUdp"));
        sizePolicy2.setHeightForWidth(btnUdp->sizePolicy().hasHeightForWidth());
        btnUdp->setSizePolicy(sizePolicy2);
        btnUdp->setMinimumSize(QSize(100, 24));
        btnUdp->setMaximumSize(QSize(100, 24));
        btnUdp->setCheckable(true);

        layout3->addWidget(btnUdp);

        spc2 = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        layout3->addItem(spc2);


        vlayout1->addWidget(box1);

        layout1 = new QGridLayout();
        layout1->setSpacing(5);
        layout1->setObjectName(QStringLiteral("layout1"));
        layout1->setContentsMargins(-1, 10, -1, -1);
        lab3 = new QLabel(ClientForm);
        lab3->setObjectName(QStringLiteral("lab3"));

        layout1->addWidget(lab3, 0, 0, 1, 1);

        btnSend0 = new QToolButton(ClientForm);
        btnSend0->setObjectName(QStringLiteral("btnSend0"));
        btnSend0->setMinimumSize(QSize(60, 24));
        btnSend0->setMaximumSize(QSize(60, 24));

        layout1->addWidget(btnSend0, 0, 2, 1, 1);

        lab4 = new QLabel(ClientForm);
        lab4->setObjectName(QStringLiteral("lab4"));

        layout1->addWidget(lab4, 1, 0, 1, 1);

        btnSend1 = new QToolButton(ClientForm);
        btnSend1->setObjectName(QStringLiteral("btnSend1"));
        btnSend1->setMinimumSize(QSize(60, 24));
        btnSend1->setMaximumSize(QSize(60, 24));

        layout1->addWidget(btnSend1, 1, 2, 1, 1);

        lab5 = new QLabel(ClientForm);
        lab5->setObjectName(QStringLiteral("lab5"));

        layout1->addWidget(lab5, 2, 0, 1, 1);

        edtBuf2 = new QLineEdit(ClientForm);
        edtBuf2->setObjectName(QStringLiteral("edtBuf2"));
        edtBuf2->setMinimumSize(QSize(0, 24));
        edtBuf2->setMaximumSize(QSize(16777215, 24));

        layout1->addWidget(edtBuf2, 2, 1, 1, 1);

        btnSend2 = new QToolButton(ClientForm);
        btnSend2->setObjectName(QStringLiteral("btnSend2"));
        btnSend2->setMinimumSize(QSize(60, 24));
        btnSend2->setMaximumSize(QSize(60, 24));

        layout1->addWidget(btnSend2, 2, 2, 1, 1);

        lab6 = new QLabel(ClientForm);
        lab6->setObjectName(QStringLiteral("lab6"));

        layout1->addWidget(lab6, 3, 0, 1, 1);

        edtBuf3 = new QLineEdit(ClientForm);
        edtBuf3->setObjectName(QStringLiteral("edtBuf3"));
        edtBuf3->setMinimumSize(QSize(0, 24));
        edtBuf3->setMaximumSize(QSize(16777215, 24));

        layout1->addWidget(edtBuf3, 3, 1, 1, 1);

        btnSend3 = new QToolButton(ClientForm);
        btnSend3->setObjectName(QStringLiteral("btnSend3"));
        btnSend3->setMinimumSize(QSize(60, 24));
        btnSend3->setMaximumSize(QSize(60, 24));

        layout1->addWidget(btnSend3, 3, 2, 1, 1);

        edtBuf0 = new QLineEdit(ClientForm);
        edtBuf0->setObjectName(QStringLiteral("edtBuf0"));
        edtBuf0->setMinimumSize(QSize(0, 24));
        edtBuf0->setMaximumSize(QSize(16777215, 24));

        layout1->addWidget(edtBuf0, 0, 1, 1, 1);

        edtBuf1 = new QLineEdit(ClientForm);
        edtBuf1->setObjectName(QStringLiteral("edtBuf1"));
        edtBuf1->setMinimumSize(QSize(0, 24));
        edtBuf1->setMaximumSize(QSize(16777215, 24));

        layout1->addWidget(edtBuf1, 1, 1, 1, 1);


        vlayout1->addLayout(layout1);

        layout2 = new QHBoxLayout();
        layout2->setSpacing(5);
        layout2->setObjectName(QStringLiteral("layout2"));
        layout2->setContentsMargins(0, 5, 0, 0);
        lab7 = new QLabel(ClientForm);
        lab7->setObjectName(QStringLiteral("lab7"));

        layout2->addWidget(lab7);

        lab8 = new QLabel(ClientForm);
        lab8->setObjectName(QStringLiteral("lab8"));

        layout2->addWidget(lab8);

        labRecv = new QLabel(ClientForm);
        labRecv->setObjectName(QStringLiteral("labRecv"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(labRecv->sizePolicy().hasHeightForWidth());
        labRecv->setSizePolicy(sizePolicy3);
#ifndef QT_NO_TOOLTIP
        labRecv->setToolTip(QStringLiteral(""));
#endif // QT_NO_TOOLTIP
        labRecv->setText(QStringLiteral("0"));

        layout2->addWidget(labRecv);

        lab9 = new QLabel(ClientForm);
        lab9->setObjectName(QStringLiteral("lab9"));

        layout2->addWidget(lab9);

        labSend = new QLabel(ClientForm);
        labSend->setObjectName(QStringLiteral("labSend"));
#ifndef QT_NO_TOOLTIP
        labSend->setToolTip(QStringLiteral(""));
#endif // QT_NO_TOOLTIP
        labSend->setText(QStringLiteral("0"));

        layout2->addWidget(labSend);

        spc3 = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        layout2->addItem(spc3);

        chkLog = new QCheckBox(ClientForm);
        chkLog->setObjectName(QStringLiteral("chkLog"));

        layout2->addWidget(chkLog);

        btnClear = new QToolButton(ClientForm);
        btnClear->setObjectName(QStringLiteral("btnClear"));
        btnClear->setMinimumSize(QSize(60, 24));
        btnClear->setMaximumSize(QSize(60, 24));

        layout2->addWidget(btnClear);


        vlayout1->addLayout(layout2);

        treeOutput = new QTreeWidget(ClientForm);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setText(0, QStringLiteral("1"));
        treeOutput->setHeaderItem(__qtreewidgetitem);
        treeOutput->setObjectName(QStringLiteral("treeOutput"));
        treeOutput->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        treeOutput->setEditTriggers(QAbstractItemView::NoEditTriggers);
        treeOutput->setProperty("showDropIndicator", QVariant(false));
        treeOutput->setAlternatingRowColors(false);
        treeOutput->setSelectionMode(QAbstractItemView::SingleSelection);
        treeOutput->setSelectionBehavior(QAbstractItemView::SelectRows);
        treeOutput->setWordWrap(true);
        treeOutput->header()->setVisible(false);
        treeOutput->header()->setStretchLastSection(true);

        vlayout1->addWidget(treeOutput);

        txtOutput = new QPlainTextEdit(ClientForm);
        txtOutput->setObjectName(QStringLiteral("txtOutput"));
        sizePolicy.setHeightForWidth(txtOutput->sizePolicy().hasHeightForWidth());
        txtOutput->setSizePolicy(sizePolicy);
        txtOutput->setMinimumSize(QSize(0, 70));
        txtOutput->setMaximumSize(QSize(16777215, 70));
        txtOutput->setAcceptDrops(false);
        txtOutput->setFrameShape(QFrame::Box);
        txtOutput->setFrameShadow(QFrame::Plain);
        txtOutput->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        txtOutput->setTabChangesFocus(true);
        txtOutput->setUndoRedoEnabled(false);
        txtOutput->setReadOnly(true);
        txtOutput->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        vlayout1->addWidget(txtOutput);

        QWidget::setTabOrder(cmbAddr, cmbPort);
        QWidget::setTabOrder(cmbPort, btnTcp);
        QWidget::setTabOrder(btnTcp, btnUdp);
        QWidget::setTabOrder(btnUdp, edtBuf0);
        QWidget::setTabOrder(edtBuf0, btnSend0);
        QWidget::setTabOrder(btnSend0, edtBuf1);
        QWidget::setTabOrder(edtBuf1, btnSend1);
        QWidget::setTabOrder(btnSend1, edtBuf2);
        QWidget::setTabOrder(edtBuf2, btnSend2);
        QWidget::setTabOrder(btnSend2, edtBuf3);
        QWidget::setTabOrder(edtBuf3, btnSend3);
        QWidget::setTabOrder(btnSend3, chkLog);
        QWidget::setTabOrder(chkLog, btnClear);
        QWidget::setTabOrder(btnClear, treeOutput);

        retranslateUi(ClientForm);

        QMetaObject::connectSlotsByName(ClientForm);
    } // setupUi

    void retranslateUi(QWidget *ClientForm)
    {
        ClientForm->setWindowTitle(QApplication::translate("ClientForm", "Client", Q_NULLPTR));
        box1->setTitle(QApplication::translate("ClientForm", "Network Setup", Q_NULLPTR));
        lab1->setText(QApplication::translate("ClientForm", "Server IP:", Q_NULLPTR));
        lab2->setText(QApplication::translate("ClientForm", "Port:", Q_NULLPTR));
#ifndef QT_NO_STATUSTIP
        btnTcp->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        btnTcp->setText(QApplication::translate("ClientForm", "TCP Connect", Q_NULLPTR));
#ifndef QT_NO_STATUSTIP
        btnUdp->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        btnUdp->setText(QApplication::translate("ClientForm", "UDP Channel", Q_NULLPTR));
        lab3->setText(QApplication::translate("ClientForm", "Buf 0:", Q_NULLPTR));
        btnSend0->setText(QApplication::translate("ClientForm", "Send", Q_NULLPTR));
        lab4->setText(QApplication::translate("ClientForm", "Buf 1:", Q_NULLPTR));
        btnSend1->setText(QApplication::translate("ClientForm", "Send", Q_NULLPTR));
        lab5->setText(QApplication::translate("ClientForm", "Buf 2:", Q_NULLPTR));
        btnSend2->setText(QApplication::translate("ClientForm", "Send", Q_NULLPTR));
        lab6->setText(QApplication::translate("ClientForm", "Buf 3:", Q_NULLPTR));
        btnSend3->setText(QApplication::translate("ClientForm", "Send", Q_NULLPTR));
        lab7->setText(QApplication::translate("ClientForm", "Output:    ", Q_NULLPTR));
        lab8->setText(QApplication::translate("ClientForm", "Recv", Q_NULLPTR));
        lab9->setText(QApplication::translate("ClientForm", ", Send", Q_NULLPTR));
        chkLog->setText(QApplication::translate("ClientForm", "Write log", Q_NULLPTR));
        btnClear->setText(QApplication::translate("ClientForm", "Clear", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ClientForm: public Ui_ClientForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLIENTFORM_H
